package com.atm.money.transfers.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.atm.money.transfers.constant.Constatnts;
import com.atm.money.transfers.model.Transaction;
import com.atm.money.transfers.service.ITransactionService;

@RestController
@RequestMapping(Constatnts.TRANSACTION_PATH)
public class TransactionController {

    @Autowired
    private ITransactionService transactionService;

    @GetMapping("{id}")
    public Transaction get(@PathVariable("id") Long id) {
        return transactionService.get(id);
    }

    @GetMapping
    public List<Transaction> getAll(@RequestParam(required = false) Long payer, @RequestParam(required = false) Long recipient) {
        return transactionService.filter(payer, recipient);
    }

    @PostMapping
    public Transaction transfer(@RequestBody @Valid Transaction transaction) {
        return transactionService.create(transaction);
    }
}