package com.atm.money.transfers.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.atm.money.transfers.constant.Constatnts;
import com.atm.money.transfers.model.Account;
import com.atm.money.transfers.service.IAccountService;

@RestController
@RequestMapping(Constatnts.ACCOUNT_PATH)
public class AccountController {

    @Autowired
    private IAccountService accountService;

    @GetMapping("{id}")
    public Account get(@PathVariable("id") Long id) {
        return accountService.get(id);
    }

    @GetMapping
    public List<Account> getAll() {
        return accountService.getAll();
    }

    @PostMapping
    public Account create(@Valid @RequestBody Account account) {
        return accountService.create(account);
    }


}
