package com.atm.money.transfers.service;

import java.util.List;

import com.atm.money.transfers.model.Transaction;

public interface ITransactionService {

    Transaction create(Transaction transaction);

    Transaction get(Long id);

    List<Transaction> filter(Long payer, Long recipient);

}