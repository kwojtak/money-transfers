package com.atm.money.transfers.service;

import java.util.List;

import com.atm.money.transfers.model.Account;

public interface IAccountService {

    Account create(Account account);

    Account get(Long id);

    List<Account> getAll();

}