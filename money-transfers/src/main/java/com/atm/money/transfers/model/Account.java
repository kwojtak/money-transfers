package com.atm.money.transfers.model;

import java.math.BigDecimal;

import javax.validation.constraints.NotNull;

public class Account {

    private Long id;
    @NotNull
    private Long clientId;
    private BigDecimal balance;

    public Account() {

    }

    public Account(Long id, Long clientId, BigDecimal balance) {
        this.id = id;
        this.setClientId(clientId);
        this.balance = balance;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getClientId() {
        return clientId;
    }

    public void setClientId(Long clientId) {
        this.clientId = clientId;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }
}
