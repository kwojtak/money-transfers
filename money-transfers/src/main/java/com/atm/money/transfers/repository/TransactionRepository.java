package com.atm.money.transfers.repository;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.springframework.stereotype.Repository;

import com.atm.money.transfers.model.Transaction;

@Repository
public class TransactionRepository {

    private final AtomicLong transactionIdGenerator;
    private final ConcurrentMap<Long, Transaction> transactions;

    public TransactionRepository() {
        this.transactionIdGenerator = new AtomicLong(0L);
        this.transactions = new ConcurrentHashMap<>();
    }

    public TransactionRepository(ConcurrentMap<Long, Transaction> transactions) {
        this.transactions = transactions;
        this.transactionIdGenerator = new AtomicLong(0L);
    }

    public List<Transaction> filter(Long payer, Long recipient) {

        if (payer == null && recipient == null) {
            return new ArrayList<>(transactions.values());
        }

        List<Predicate<Transaction>> predicates = new ArrayList<Predicate<Transaction>>();

        if(payer != null) {
            predicates.add(e -> e.getPayer().equals(payer));
        }

        if(recipient != null) {
            predicates.add(e -> e.getRecipient().equals(recipient));
        }

        return transactions.values().stream()
                .filter(predicates.stream().reduce(x -> true, Predicate::and))
                .collect(Collectors.toList());
    }

    public Transaction get(Long id) {
        return transactions.get(id);
    }

    public Transaction create(Transaction transaction) {
        transaction.setId(transactionIdGenerator.getAndIncrement());
        transactions.put(transaction.getId(), transaction);
        return transaction;
    }

}
