package com.atm.money.transfers.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;

public class Transaction {

    private Long id;
    @NotNull
    @PositiveOrZero
    private BigDecimal amount;
    @NotNull
    private String title;
    @NotNull
    private Long payer;
    @NotNull
    private Long recipient;
    private Date date;

    public Transaction() {
    }

    public Transaction(Long id, BigDecimal amount, String title, Long payer, Long recipient, Date date) {
        this.id = id;
        this.amount = amount;
        this.title = title;
        this.payer = payer;
        this.recipient = recipient;
        this.date = date;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getPayer() {
        return payer;
    }

    public void setPayer(Long payer) {
        this.payer = payer;
    }

    public Long getRecipient() {
        return recipient;
    }

    public void setRecipient(Long recipient) {
        this.recipient = recipient;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

}
