package com.atm.money.transfers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.atm.money.transfers.service.DummyDataGenerator;

@Component
public class CommandLineAppStartupRunner implements CommandLineRunner {

    @Autowired
    private DummyDataGenerator dataGenerator;

    @Override
    public void run(String... args) throws Exception {
        dataGenerator.generateData();
    }
}