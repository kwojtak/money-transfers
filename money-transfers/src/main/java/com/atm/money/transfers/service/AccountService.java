package com.atm.money.transfers.service;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.atm.money.transfers.model.Account;
import com.atm.money.transfers.repository.AccountRepository;

@Service
public class AccountService implements IAccountService {

    @Autowired
    private AccountRepository accountRepository;

    @Override
    public Account create(Account account) {
        if (account.getBalance() == null) {
            account.setBalance(new BigDecimal(0));
        }
        return accountRepository.create(account);
    }

    @Override
    public Account get(Long id) {
        return accountRepository.get(id);
    }

    @Override
    public List<Account> getAll() {
        return accountRepository.getAll();
    }

}
