package com.atm.money.transfers.repository;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicLong;

import org.springframework.stereotype.Repository;

import com.atm.money.transfers.model.Account;

@Repository
public class AccountRepository {

    private final AtomicLong accountIdGenerator;
    private final ConcurrentMap<Long, Account> accounts;

    public AccountRepository() {
        this.accountIdGenerator = new AtomicLong(0L);
        this.accounts = new ConcurrentHashMap<>();
    }

    public List<Account> getAll() {
        return new ArrayList<Account>(accounts.values());
    }

    public Account get(Long id) {
        return accounts.get(id);
    }

    public Account create(Account account) {
        account.setId(accountIdGenerator.getAndIncrement());
        accounts.put(account.getId(), account);
        return account;
    }

}
