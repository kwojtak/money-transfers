package com.atm.money.transfers.service;

import java.math.BigDecimal;
import java.util.stream.LongStream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.atm.money.transfers.model.Account;
import com.atm.money.transfers.repository.AccountRepository;

@Component
public class DummyDataGenerator {

    @Autowired
    private AccountRepository accountRepository;

    public void generateData() {
        generateAccounts();
    }

    private void generateAccounts() {
        LongStream.range(1, 10).forEach(clientId -> accountRepository.create(new Account(0l, clientId, new BigDecimal(100))));
    }

}
