package com.atm.money.transfers.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.atm.money.transfers.exception.TransactionException;
import com.atm.money.transfers.model.Account;
import com.atm.money.transfers.model.Transaction;
import com.atm.money.transfers.repository.AccountRepository;
import com.atm.money.transfers.repository.TransactionRepository;

@Service
public class TransactionService implements ITransactionService {

    @Autowired
    private TransactionRepository transactionRepository;
    @Autowired
    private AccountRepository accountRepository;

    @Override
    public synchronized Transaction create(Transaction transaction) {
        validate(transaction);
        updateBalances(transaction);
        transaction.setDate(new Date());
        return transactionRepository.create(transaction);
    }

    private void updateBalances(Transaction transaction) {
        Account payer = accountRepository.get(transaction.getPayer());
        payer.setBalance(payer.getBalance().subtract(transaction.getAmount()));
        Account recipient = accountRepository.get(transaction.getRecipient());
        recipient.setBalance(recipient.getBalance().add(transaction.getAmount()));
    }

    private void validate(Transaction transaction) {
        if (transaction.getRecipient().equals(transaction.getPayer())) {
            throw new TransactionException("Recipient cannot be the same as payer");
        }

        if (accountRepository.get(transaction.getRecipient()) == null) {
            throw new TransactionException("Recipient not found");
        }

        Account payerAccount = accountRepository.get(transaction.getPayer());
        if (payerAccount == null) {
            throw new TransactionException("Payer not found");
        }

        if (payerAccount.getBalance().compareTo(transaction.getAmount()) < 0) {
            throw new TransactionException("Funds of payer are not enough");
        }

    }

    @Override
    public Transaction get(Long id) {
        return transactionRepository.get(id);
    }

    @Override
    public List<Transaction> filter(Long payer, Long recipient) {
        return transactionRepository.filter(payer, recipient);
    }
}
