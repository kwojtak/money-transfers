package com.atm.money.transfers.repositiry;

import static org.assertj.core.api.Assertions.assertThat;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ConcurrentMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.Before;
import org.junit.Test;

import com.atm.money.transfers.model.Transaction;
import com.atm.money.transfers.repository.TransactionRepository;

public class TransactionRepositoryTest {

    private TransactionRepository transactionRepository;

    private final static Long ACCOUNT_A_ID = 11l;
    private final static Long ACCOUNT_B_ID = 12l;
    private final static Long ACCOUNT_C_ID = 13l;

    private Transaction tAB;
    private Transaction tAC;
    private Transaction tBA;
    private Transaction tBC;
    private Transaction tCA;
    private Transaction tCB;

    @Before
    public void before() {
        tAB = new Transaction(1l, new BigDecimal(20), "test", ACCOUNT_A_ID, ACCOUNT_B_ID, new Date());
        tAC = new Transaction(2l, new BigDecimal(20), "test", ACCOUNT_A_ID, ACCOUNT_C_ID, new Date());
        tBA = new Transaction(3l, new BigDecimal(20), "test", ACCOUNT_B_ID, ACCOUNT_A_ID, new Date());
        tBC = new Transaction(4l, new BigDecimal(20), "test", ACCOUNT_B_ID, ACCOUNT_C_ID, new Date());
        tCA = new Transaction(5l, new BigDecimal(20), "test", ACCOUNT_C_ID, ACCOUNT_A_ID, new Date());
        tCB = new Transaction(6l, new BigDecimal(20), "test", ACCOUNT_C_ID, ACCOUNT_B_ID, new Date());

        ConcurrentMap<Long, Transaction> testTransactions = Stream.of(tAB, tAC, tBA, tBC, tCA, tCB)
                .collect(Collectors.toConcurrentMap(Transaction::getId, t -> t));

        transactionRepository = new TransactionRepository(testTransactions);
    }

    @Test
    public void shouldGetAll() {
        List<Transaction> result = transactionRepository.filter(null, null);

        assertThat(result).hasSize(6).extracting("id").contains(tAB.getId(), tAC.getId(), tBA.getId(), tBC.getId(), tCA.getId(), tCB.getId());
    }

    @Test
    public void shouldGetDebitsForGivenAccount() {
        List<Transaction> result = transactionRepository.filter(ACCOUNT_A_ID, null);

        assertThat(result).hasSize(2).extracting("id").contains(tAB.getId(), tAC.getId());
    }

    @Test
    public void shouldGetCreditsForGivenAccount() {
        List<Transaction> result = transactionRepository.filter(null, ACCOUNT_A_ID);

        assertThat(result).hasSize(2).extracting("id").contains(tBA.getId(), tCA.getId());
    }

    @Test
    public void shouldGetDebitsAndCreditsForGivenAccount() {
        List<Transaction> result = transactionRepository.filter(ACCOUNT_A_ID, ACCOUNT_B_ID);

        assertThat(result).hasSize(1).extracting("id").contains(tAB.getId());
    }

}
