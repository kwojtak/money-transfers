package com.atm.money.transfers.controller;

import static com.atm.money.transfers.controller.ControllerTestUtil.convertObjectToJson;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.math.BigDecimal;
import java.util.List;

import org.assertj.core.util.Lists;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.atm.money.transfers.constant.Constatnts;
import com.atm.money.transfers.model.Account;
import com.atm.money.transfers.service.IAccountService;

@RunWith(SpringRunner.class)
@WebMvcTest(AccountController.class)
public class AccountControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private IAccountService accountService;

    @Test
    public void shouldGetById() throws Exception {
        Account account = new Account(1l, 20l, new BigDecimal(20));
        Mockito.when(accountService.get(account.getId())).thenReturn(account);

        mvc.perform(get(Constatnts.ACCOUNT_PATH + '/' + account.getId()).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(content().json(convertObjectToJson(account)));
    }

    @Test
    public void shouldGetAll() throws Exception {
        Account account1 = new Account(1l, 10l, new BigDecimal(10));
        Account account2 = new Account(2l, 20l, new BigDecimal(20));
        List<Account> accounts = Lists.list(account1, account2);

        Mockito.when(accountService.getAll()).thenReturn(accounts);

        mvc.perform(get(Constatnts.ACCOUNT_PATH).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(content().json(convertObjectToJson(accounts)));
    }

    @Test
    public void shouldCreate() throws Exception {
        Account account = new Account();
        account.setBalance(new BigDecimal(20));
        account.setClientId(20l);

        Account createdAccount = new Account(2l, 20l, new BigDecimal(20l));
        Mockito.when(accountService.create(Mockito.any())).thenReturn(createdAccount);


        mvc.perform(post(Constatnts.ACCOUNT_PATH).content(convertObjectToJson(account)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(content().json(convertObjectToJson(createdAccount)));
    }

    @Test
    public void shouldReturnBadRequestIfClientIdMissingWhenCreate() throws Exception {
        Account account = new Account();
        account.setBalance(new BigDecimal(20));


        mvc.perform(post(Constatnts.ACCOUNT_PATH).content(convertObjectToJson(account)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

}
