package com.atm.money.transfers.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import com.atm.money.transfers.exception.TransactionException;
import com.atm.money.transfers.model.Account;
import com.atm.money.transfers.model.Transaction;
import com.atm.money.transfers.repository.AccountRepository;
import com.atm.money.transfers.repository.TransactionRepository;

@RunWith(SpringRunner.class)
public class TransactionServiceTest {

    @MockBean
    private TransactionRepository transactionRepositoryMock;
    @MockBean
    private AccountRepository accountRepositoryMock;

    @TestConfiguration
    static class TransactionServiceTestContextConfiguration {

        @Bean
        public ITransactionService employeeService() {
            return new TransactionService();
        }
    }

    @Autowired
    private ITransactionService transactionService;

    @Before
    public void before() {
        when(transactionRepositoryMock.create(Mockito.any(Transaction.class))).thenAnswer(i -> i.getArguments()[0]);
    }

    @Test
    public void shouldCreateTransaction() {
        Account dummyAccount = new Account(111l, 12l, new BigDecimal(100));
        Mockito.when(accountRepositoryMock.get(Mockito.anyLong())).thenReturn(dummyAccount);

        Transaction transaction = new Transaction();
        transaction.setAmount(new BigDecimal(10.99));
        transaction.setTitle("title");
        transaction.setPayer(111l);
        transaction.setRecipient(222l);

        Transaction result = transactionService.create(transaction);


        Mockito.verify(transactionRepositoryMock).create(Mockito.any());
        assertNotNull(result);
        assertEquals(transaction, result);
        assertNotNull("Date not defined", result.getDate());
        assertEquals("Incorrect amount", transaction.getAmount(), result.getAmount());
        assertEquals("Incorrect title", transaction.getTitle(), result.getTitle());
        assertEquals("Incorrect payer", transaction.getPayer(), result.getPayer());
        assertEquals("Incorrect recipient", transaction.getRecipient(), result.getRecipient());
    }

    @Test(expected = TransactionException.class)
    public void shouldFailIfInvalidRecipientForTransfer() {
        Account payerAccount = new Account(111l, 12l, new BigDecimal(100));
        Mockito.when(accountRepositoryMock.get(payerAccount.getId())).thenReturn(payerAccount);

        Transaction transaction = new Transaction();
        transaction.setRecipient(14l);
        transaction.setPayer(payerAccount.getId());

        transactionService.create(transaction);
    }

    @Test(expected = TransactionException.class)
    public void shouldFailIfInvalidPayerForTransfer() {
        Account recipientAccount = new Account(111l, 12l, new BigDecimal(100));
        Mockito.when(accountRepositoryMock.get(recipientAccount.getId())).thenReturn(recipientAccount);

        Transaction transaction = new Transaction();
        transaction.setRecipient(recipientAccount.getId());
        transaction.setPayer(123l);

        transactionService.create(transaction);
    }

    @Test(expected = TransactionException.class)
    public void shouldFailIfPayerNotHaveEnoughFundsWhenTransfer() {
        Account payerAccount = new Account(111l, 12l, new BigDecimal(5));
        Account recipientAccount = new Account(112l, 13l, new BigDecimal(100));
        Mockito.when(accountRepositoryMock.get(payerAccount.getId())).thenReturn(payerAccount);
        Mockito.when(accountRepositoryMock.get(recipientAccount.getId())).thenReturn(recipientAccount);

        Transaction transaction = new Transaction();
        transaction.setRecipient(recipientAccount.getId());
        transaction.setPayer(payerAccount.getId());
        transaction.setAmount(new BigDecimal(100.10));

        transactionService.create(transaction);
    }

    @Test(expected = TransactionException.class)
    public void shouldFailIfPayerAndRecipientAreTheSameWhenTransfer() {
        Account dummyAccount = new Account(111l, 12l, new BigDecimal(100));
        Mockito.when(accountRepositoryMock.get(Mockito.anyLong())).thenReturn(dummyAccount);

        Transaction transaction = new Transaction();
        transaction.setRecipient(dummyAccount.getId());
        transaction.setPayer(dummyAccount.getId());
        transaction.setAmount(new BigDecimal(2.10));

        transactionService.create(transaction);
    }

    @Test
    public void shouldDecreasePayorBalanceAfterTransfer() {
        BigDecimal payorInitBalance = new BigDecimal(50);
        Account payerAccount = new Account(111l, 12l, payorInitBalance);
        Account recipientAccount = new Account(112l, 13l, new BigDecimal(100));
        Mockito.when(accountRepositoryMock.get(payerAccount.getId())).thenReturn(payerAccount);
        Mockito.when(accountRepositoryMock.get(recipientAccount.getId())).thenReturn(recipientAccount);

        Transaction transaction = new Transaction();
        transaction.setRecipient(recipientAccount.getId());
        transaction.setPayer(payerAccount.getId());
        transaction.setAmount(new BigDecimal(2.1));

        transactionService.create(transaction);

        assertEquals(payorInitBalance.subtract(transaction.getAmount()), payerAccount.getBalance());
    }

    @Test
    public void shouldIncreaseReceipientBalanceAfterTransfer() {
        BigDecimal recipientInitBalance = new BigDecimal(100);
        Account payerAccount = new Account(111l, 12l, new BigDecimal(50));
        Account recipientAccount = new Account(112l, 13l, recipientInitBalance);
        Mockito.when(accountRepositoryMock.get(payerAccount.getId())).thenReturn(payerAccount);
        Mockito.when(accountRepositoryMock.get(recipientAccount.getId())).thenReturn(recipientAccount);

        Transaction transaction = new Transaction();
        transaction.setRecipient(recipientAccount.getId());
        transaction.setPayer(payerAccount.getId());
        transaction.setAmount(new BigDecimal(2.1));

        transactionService.create(transaction);

        assertEquals(recipientInitBalance.add(transaction.getAmount()), recipientAccount.getBalance());

    }

}
