package com.atm.money.transfers.controller;

import static com.atm.money.transfers.controller.ControllerTestUtil.convertObjectToJson;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.assertj.core.util.Lists;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.atm.money.transfers.constant.Constatnts;
import com.atm.money.transfers.exception.TransactionException;
import com.atm.money.transfers.model.Transaction;
import com.atm.money.transfers.service.ITransactionService;

@RunWith(SpringRunner.class)
@WebMvcTest(TransactionController.class)
public class TransactionControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private ITransactionService transactionService;

    @Test
    public void shouldGetById() throws Exception {
        Transaction transaction = new Transaction(2l, new BigDecimal(2), "operation1", 1l, 2l, new Date());
        Mockito.when(transactionService.get(transaction.getId())).thenReturn(transaction);

        mvc.perform(get(Constatnts.TRANSACTION_PATH + '/' + transaction.getId()).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(content().json(convertObjectToJson(transaction)));
    }

    @Test
    public void shouldGetAll() throws Exception {
        Transaction transaction1 = new Transaction(1l, new BigDecimal(1), "operation1", 1l, 2l, new Date());
        Transaction transaction2 = new Transaction(2l, new BigDecimal(2), "operation1", 2l, 3l, new Date());
        List<Transaction> transactions = Lists.list(transaction1, transaction2);

        Mockito.when(transactionService.filter(null, null)).thenReturn(transactions);

        mvc.perform(get(Constatnts.TRANSACTION_PATH).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(content().json(convertObjectToJson(transactions)));
    }

    @Test
    public void shouldCreate() throws Exception {
        Transaction transaction = new Transaction();
        transaction.setAmount(new BigDecimal(1));
        transaction.setPayer(1l);
        transaction.setRecipient(2l);
        transaction.setTitle("o1");

        Transaction createdTransaction = new Transaction(1l, new BigDecimal(1), "operation1", 1l, 2l, new Date());
        Mockito.when(transactionService.create(Mockito.any())).thenReturn(createdTransaction);


        mvc.perform(post(Constatnts.TRANSACTION_PATH).content(convertObjectToJson(transaction)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(content().json(convertObjectToJson(createdTransaction)));
    }

    @Test
    public void shouldReturnBadRequestIfNotValidTransactionWhenCreate() throws Exception {
        Transaction transaction = new Transaction();
        transaction.setAmount(new BigDecimal(1));
        transaction.setPayer(1l);
        transaction.setTitle("o1");


        mvc.perform(post(Constatnts.TRANSACTION_PATH).content(convertObjectToJson(transaction)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void shouldReturnBadRequestIfServiceValidationFailsWhenCreate() throws Exception {
        Transaction transaction = new Transaction();
        transaction.setAmount(new BigDecimal(1));
        transaction.setPayer(1l);
        transaction.setRecipient(2l);
        transaction.setTitle("o1");

        Mockito.when(transactionService.create(Mockito.any())).thenThrow(new TransactionException());

        mvc.perform(post(Constatnts.TRANSACTION_PATH).content(convertObjectToJson(transaction)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

}
