package com.atm.money.transfers.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;

public final class ControllerTestUtil {

    private ControllerTestUtil() {

    }

    public static String convertObjectToJson(Object object) throws JsonProcessingException {
        ObjectWriter writer = new ObjectMapper().configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false)
                .writerWithDefaultPrettyPrinter();
        return writer.writeValueAsString(object);
    }

}
