# money-transfers

# Requirements:
- Language: Java.
- Keep it simple and to the point (e.g. no need to implement any authentication).
- Assume the API is invoked by multiple systems and services on behalf of end users.
- You can use frameworks/libraries if you like. (For DI and REST use Spring Framework)
- The datastore should run in map implementation (in-memory).
- The final result should be executable as a standalone program (should not require a pre-installed container/server).
- Demonstrate with tests that the API works as expected


# Assumptions
- no support for currency (all transactions are done using abstract sums)
- no authentication & authorization
- only succeed transactions are stored

# Use cases
- transfer money between two accounts
- unable to transfer money if funds are not sufficient
- check balance of given account
- add new account
- get details of given transaction
- get list of all transactions
- get incoming transactions
- get outgoing transactions


# REST API

 - *GET /accounts* get all accounts
 - *GET /accounts/{id}* get account with id = {id}
 - *POST /accounts* create new account according to request body
 
 - *GET /transactions* get all transactions
 - *GET /transactions/{id}* get transaction with id = {id}
 - *GET /transactions?recipient={recipientId}* get credits for account with id = {recipientId}
 - *GET /transactions?payer={payerId}* get debits for account with id = {payerId}
 - *GET /transactions?payer={payerId}&recipient={recipientId}* get transactions between account with id = {payerId} and {recipientId}
 - *POST /transactions* transfer money according to request body
 
